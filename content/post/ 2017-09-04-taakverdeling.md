---
title: Mijn eerste idee
subtitle: 04-09-2017
date: 2017-09-04
tags: ["blog", "taakverdeling", "post","marja"]
---


Mijn projectgroepje is bij elkaar gekomen op school om de ideeën aan elkaar te presenteren, ik kon hier helaas niet bij zijn. Ik heb school gebeld om mijn situatie uit te leggen en naar oplossingen te zoeken, aangezien ik geen mailtje terug heb gehad. Ik ben verteld dat ik mijn studieloopbaancoach nogmaals moet mailen, dit heb ik meteen gedaan.
 Om 15:00 vond het eerste Skypegesprek plaats, hiervoor had ik een aantal vragen voorbereid die inmiddels zijn beantwoord:
Wat is het idee geworden of hoe ver zijn we nu met het idee? Het idee van Jesse is ons idee geworden, deze zal worden geüpload in Trello.
Welke doelgroep gebruiken we? De eerstejaars van de opleiding gaming. Dit wordt verder uitgelegd in Trello (Jesse’s idee).
Is er naar mijn idee gekeken en wat vonden jullie ervan? Iedereen heeft elkaars ideeën beoordeeld en het is die van Jesse geworden, deze sprong eruit.
Wat is de taakverdeling in onze projectgroep? Jesse is de groepsleider aangezien wij zijn idee hebben gekozen, de rest moet nog worden gekozen.
Is er al een samenwerkingscontract? Zo nee, ik kan deze maken? Deze is al gemaakt door Indra en zal worden geüpload in Trello.
Welk programma gebruiken wij om alle gezamenlijke documenten in te uploaden? Trello wordt gebruikt, ik heb mijn e-mail adres hiervoor gegeven.

Hierna is de taakverdeling aangegeven op Trello, ik heb deze doorgenomen en ik heb hier een planning van gemaakt voor iteratie 1.  
