---
title: Eerste dag
subtitle: 31-08-2017
date: 2017-08-31
tags: ["blog", "nieuw", "post"]
---
Ik heb mijn projectgroepje leren kennen door middel van een kennismakingsspel in Rotterdam, georganiseerd door de opleiding CMD. Ook kregen we deze dag te horen wat onze eerste opdracht is voor de eerste design challenge. We hebben toen meteen met elkaar afgesproken maandag 04-09-2017 allemaal 1 idee uitgewerkt te hebben voor de design challenge.