---
title: Mijn eerste idee
subtitle: 03-09-2017
date: 2017-09-03
tags: ["blog", "idee", "post","eerste"]
---

Ik heb nagedacht over ideeën voor de design challenge, uiteindelijk is hier één idee uitgekomen. Deze heb ik uitgewerkt op een A4-tje en gestuurd naar mijn projectgroepje. Mijn gekozen doelgroep zijn alle 1e jaar studenten studerend aan de Willem de Kooning Academie. Mijn idee is als volgt:                                                                                                 
Alle studenten moeten de applicatie 'Intro WKA' downloaden, dit is gelijk het online interactieve element. Wanneer deze is gedownload, maakt iedere student een eigen account aan die gekoppeld is aan de naam van de student. Iedere student beantwoord 6 persoonlijke vragen via de applicatie. Wanneer iedere student alle vragen heeft beantwoord worden de studenten in groepjes van 5 personen verdeeld.          
Ieder groepslid zorgt ervoor dat ze in de juiste groep zitten in de applicatie, vervolgens kan het spel starten. De app geeft aan wat het eerste kunstwerk is, hier moeten de studenten heenlopen in Rotterdam. Wanneer de studenten zijn aangekomen bij het eerste kunstwerk moeten ze hier een foto van maken, deze moeten zij uploaden in de applicatie. Door middel van locatievoorzieningen moet het kunstwerk worden opgeslagen, hiervoor krijgt iedere student 2 punten.    
Vervolgens krijgt iedere student één van de 6 persoonlijke vragen over de studenten inclusief antwoord te zien in de applicatie. Iedere student moet in zijn eigen applicatie aanklikken van wie hij denkt dat het antwoord op de vraag hoort te zijn. Iedere keer dat een groepslid de juiste persoon bij het juiste antwoord heeft gekoppeld krijgt 2 punten. Uiteindelijk is er dus 1 groepswinnaar die maximaal 12 punten kan hebben en is er een groepje die maximaal 60 punten kan winnen. Het offline tastbare element is dat alle studenten langs 6 kunstwerken in Rotterdam lopen, hier maken zij foto’s van en deze moeten zij uploaden. 
